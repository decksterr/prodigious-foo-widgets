import React, { Component } from 'react';
import { connect } from 'react-redux';
import SelectLanguage from '../components/Select/Language';
import translateFn from '../helpers/translator';

const mapStateToProps = (state, props) => ({
    language: state.i18n.language
});

const withTranslation = (WrappedComponent) => {
    class I18n extends Component {
        shouldComponentUpdate(nextProps, nextState) {
            return nextProps.language !== this.props.language;
        }

        fetchLanguage = (props) => {
            return props.language || window.localStorage.language;
        };

        render() {
            return (
                <div>
                    <SelectLanguage />
                    <WrappedComponent
                        __={translateFn(this.fetchLanguage(this.props))}
                        {...this.props}
                    />
                </div>
            );
        }
    }
    return connect(mapStateToProps)(I18n);
};

export default withTranslation;
