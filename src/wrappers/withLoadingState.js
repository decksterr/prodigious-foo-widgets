import React from 'react';
import { connect } from 'react-redux';

const mapStateToProps = (state, props) => ({
    xhrState: state.xhr.xhrState
});

const detectXhrState = (WrappedComponent) => {
    return function XhrAware(props) {
        if (props.xhrState === 'loading') {
            return <WrappedComponent loading={true} {...props} />;
        }
        return <WrappedComponent {...props} />;
    };
};

const withLoadingState = (WrappedComponent) => {
    return connect(mapStateToProps)(detectXhrState(WrappedComponent));
};

export default withLoadingState;
