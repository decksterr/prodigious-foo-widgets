import React, { Component } from 'react';
import { Tab } from 'semantic-ui-react';
import FileUploads from '../components/Stats/FileUploads';
import withTranslation from '../wrappers/withTranslation';

class Widget3 extends Component {
    render() {
        return (
            <div className="widget layout__center--full-height--responsive-plz">
                <Tab
                    panes={[
                        {
                            menuItem: this.props.__('Diagram Stats'),
                            render: () => <FileUploads {...this.props} />
                        },
                        {
                            menuItem: this.props.__('Month Report'),
                            render: () => (
                                <div className="card-tab layout__center">
                                    <span>{this.props.__('Here be some monthly report')}</span>
                                </div>
                            )
                        }
                    ]}
                />
            </div>
        );
    }
}

export default withTranslation(Widget3);
