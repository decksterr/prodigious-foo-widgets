import React from 'react';

const NotFoundPage = () => (
    <div className="not-found">
        404!
    </div>
);

export default NotFoundPage;
