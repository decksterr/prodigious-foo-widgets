import React, { Component } from 'react';
import { Card } from 'semantic-ui-react';
import Dashboard from '../components/Stats/Dashboard';
import withTranslation from '../wrappers/withTranslation';

class Widget4 extends Component {
    render() {
        return (
            <div className="widget layout__center--full-height--responsive-plz">
                <div className="dashboard__container">
                    <Card fluid={true} className="dashboard__card">
                        <Dashboard {...this.props}/>
                    </Card>
                </div>
            </div>
        );
    }
}

export default withTranslation(Widget4);
