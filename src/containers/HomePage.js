import React, { Component } from 'react';
import { Grid } from 'semantic-ui-react';
import { Link } from 'react-router-dom';

const HomePage = () => (
    <div className="home layout__center--full-height">
        <Grid>
            <Grid.Row>
                <Link to="/widget-1"><strong>Widget 1</strong></Link>
            </Grid.Row>
            <Grid.Row>
                <Link to="/widget-2"><strong>Widget 2</strong></Link>
            </Grid.Row>
            <Grid.Row>
                <Link to="/widget-3"><strong>Widget 3</strong></Link>
            </Grid.Row>
            <Grid.Row>
                <Link to="/widget-4"><strong>Widget 4</strong></Link>
            </Grid.Row>
        </Grid>
    </div>
);

export default HomePage;
