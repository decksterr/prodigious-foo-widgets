const donutReducerDefaultState = { sliceData: {}, activeSlice: 2 };

export default (state = donutReducerDefaultState, action) => {
    switch (action.type) {
        case 'CHANGE_SLICE':
            return {
                ...state,
                sliceData: action.sliceData
            };
        case 'TRIGGER_SLICE':
            return {
                ...state,
                activeSlice: action.sliceId
            };
        default:
            return state;
    }
};
