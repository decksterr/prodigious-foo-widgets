const xhrReducerDefaultState = { xhrState: null };

export default (state = xhrReducerDefaultState, action) => {
    switch (action.type) {
        case 'XHR_STATE':
            return {
                xhrState: action.state
            };
        default:
            return state;
    }
};
