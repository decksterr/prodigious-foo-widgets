const translateReducerDefaultState = { language: null };

export default (state = translateReducerDefaultState, action) => {
    switch (action.type) {
        case 'TRANSLATE_TO':
            return {
                language: action.language
            };
        default:
            return state;
    }
};
