import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import SW from './services/ServiceWorker';
import Polyfill from './interfaces/Polyfills';
import AppRouter from './routers/AppRouter';
import configureStore from './store/configureStore';

const store = configureStore();

Polyfill
    .raf()
    .localStorage()
    .tapEvent();

ReactDOM.render(
    <Provider store={store}>
        <AppRouter />
    </Provider>,
    document.getElementById('root')
);
SW();
