export const translateTo = (language) => ({
    type: 'TRANSLATE_TO',
    language
});
