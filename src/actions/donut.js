export const changeSlice = (sliceData) => ({
    type: 'CHANGE_SLICE',
    sliceData
});

export const triggerSlice = (sliceId) => ({
    type: 'TRIGGER_SLICE',
    sliceId
});
