import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Skeleton extends Component {
    static propTypes = {
        height: PropTypes.number,
        style: PropTypes.object
    };

    static defaultProps = {
        height: 1,
        style: {}
    };

    getStyle = () => ({
        height: `${this.props.height}em`,
        ...this.props.style
    });

    render() {
        return <div className="loading-skeleton" style={this.getStyle()} />;
    }
}

export default Skeleton;
