import React, { Component } from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import { Grid, Icon } from 'semantic-ui-react';
import BigFatDonut, { colors } from '../Charts/BigFatDonut';
import Skeleton from '../../components/Loading/Skeleton';
import withLoadingState from '../../wrappers/withLoadingState';
import { xhrState } from '../../actions/xhr';
import { triggerSlice } from '../../actions/donut';

class FileUploads extends Component {
    state = {
        data: [],
        audio: 0,
        video: 0,
        photo: 0
    };

    componentDidMount() {
        this.props.dispatch(xhrState('loading'));
        // mocking some crappy response time here
        setTimeout(() => {
            axios.get('/data/file-uploads.json').then((res) => {
                let fileQuantities = {};
                res.data.files.map((file) => {
                    Object.assign(fileQuantities, {[file.name]: file.quantity});
                });
                this.setState(() => ({ data: res.data.files, ...fileQuantities }));
                this.props.dispatch(xhrState('loaded'));
            });
        }, 3000);
    }

    render() {
        return (
            <div className="card-tab">
                <Grid columns={3}>
                    <Grid.Row>
                        <Grid.Column width={16}>
                            <h3 className="stats-donut__title">{this.props.__('Data transfer')}</h3>
                            {this.props.loading ? (
                                <Skeleton height={10} />
                            ) : (
                                <BigFatDonut data={this.state.data} __={this.props.__} />
                            )}
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row className="stats-donut__caption-container">
                        <Grid.Column
                            className={`stats-donut__caption ${this.props.sliceData.id === 1 ? 'active--1' : ''}`}
                            style={{ borderColor: `${colors.green}` }}
                            onMouseEnter={() => this.props.dispatch(triggerSlice(1))}
                        >
                            <div className="stats">
                                <div className="stats__label">{this.props.__('audio')}</div>
                                {this.props.loading ? (
                                    <Skeleton height={2} />
                                ) : (
                                    <div className="stats__value">{this.state.audio}%</div>
                                )}
                            </div>
                        </Grid.Column>
                        <Grid.Column
                            className={`stats-donut__caption ${this.props.sliceData.id === 2 ? 'active--2' : ''}`}
                            style={{ borderColor: `${colors.red}` }}
                            onMouseEnter={() => this.props.dispatch(triggerSlice(2))}
                        >
                            <div className="stats">
                                <div className="stats__label">{this.props.__('video')}</div>
                                {this.props.loading ? (
                                    <Skeleton height={2} />
                                ) : (
                                    <div className="stats__value">{this.state.video}%</div>
                                )}
                            </div>
                        </Grid.Column>
                        <Grid.Column
                            className={`stats-donut__caption ${this.props.sliceData.id === 3 ? 'active--3' : ''}`}
                            style={{ borderColor: `${colors.yellow}` }}
                            onMouseEnter={() => this.props.dispatch(triggerSlice(3))}
                        >
                            <div className="stats">
                                <div className="stats__label">{this.props.__('photo')}</div>
                                {this.props.loading ? (
                                    <Skeleton height={2} />
                                ) : (
                                    <div className="stats__value">{this.state.photo}%</div>
                                )}
                            </div>
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row className="card__bottom-actions--nav">
                        <Grid.Column className="card__action--nav active">
                            <a>
                                <Icon name="cloud upload" size="big" />
                                <div className="card__action--nav__label">{this.props.__('Upload files')}</div>
                            </a>
                        </Grid.Column>
                        <Grid.Column className="card__action--nav">
                            <a>
                                <Icon name="share alternate" size="big" />
                                <div className="card__action--nav__label">{this.props.__('Share link')}</div>
                            </a>
                        </Grid.Column>
                        <Grid.Column className="card__action--nav">
                            <a>
                                <Icon name="hand spock" size="big" />
                                <div className="card__action--nav__label">{this.props.__('Backup')}</div>
                            </a>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </div>
        );
    }
}

const mapStateToProps = (state, props) => ({
    activeSlice: state.donut.activeSlice,
    sliceData: state.donut.sliceData
});

export default connect(mapStateToProps)(withLoadingState(FileUploads));
