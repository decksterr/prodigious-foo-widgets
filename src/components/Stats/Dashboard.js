import React, { Component } from 'react';
import axios from 'axios';
import { Grid } from 'semantic-ui-react';
import SlimShaddyLine from '../Charts/SlimShaddyLine';
import NineBarGraph from '../Charts/NineBarGraph';
import Skeleton from '../../components/Loading/Skeleton';
import withLoadingState from '../../wrappers/withLoadingState';
import { xhrState } from '../../actions/xhr';
import { formatNumber } from '../../helpers/filters';

class Dashboard extends Component {
    state = {
        line: {},
        bars: []
    };

    componentDidMount() {
        this.props.dispatch(xhrState('loading'));
        // mocking some crappy response time here
        setTimeout(() => {
            axios.get('/data/dashboard.json').then((res) => {
                console.log('loaded data', res.data);
                this.setState(() => ({ line: res.data.line, bars: res.data.bars }));
                this.props.dispatch(xhrState('loaded'));
            });
        }, 3000);
    }

    render() {
        return (
            <div className="dashboard">
                <div className="dashboard__header">
                    <div className="dashboard__subheader" />
                    {!this.props.loading && (
                        <div className="dashboard__featured">
                            <div className="stats__value">634.39</div>
                            <div className="stats__description">+2.18 (3.71%)</div>
                        </div>
                    )}
                    {!this.props.loading && <SlimShaddyLine data={this.state.line} />}
                </div>
                <div className="dashboard__content">
                    <Grid columns={2}>
                        <Grid.Row>
                            <Grid.Column width={16}>
                                <div className="dashboard__date">
                                    {this.props.__('Today')} 1:25 <small>AM</small>
                                </div>
                                <div className="dashboard__place">Bogotá D.C., Colombia</div>
                            </Grid.Column>
                        </Grid.Row>
                        <Grid.Row>
                            <Grid.Column>
                                <div className="stats dashboard__stat-main">
                                    {this.props.loading ? (
                                        <Skeleton height={3} style={{width: '8em', maxWidth: 'calc(100% - 4em)', margin: '0 2em'}} />
                                    ) : (
                                        <div className="stats__value">13.5&nbsp;M</div>
                                    )}
                                    <div className="stats__label">{this.props.__('Shares Traded')}</div>
                                </div>
                            </Grid.Column>
                            <Grid.Column>
                                <div className="stats dashboard__stat-main">
                                    {this.props.loading ? (
                                        <Skeleton height={3} style={{width: '8em', maxWidth: 'calc(100% - 4em)', margin: '0 2em'}} />
                                    ) : (
                                        <div className="stats__value">28.44&nbsp;B</div>
                                    )}
                                    <div className="stats__label">{this.props.__('Market Cap')}</div>
                                </div>
                            </Grid.Column>
                        </Grid.Row>
                        <Grid.Row>
                            <Grid.Column>
                                <div className="dashboard__label">{this.props.__('AAPL')}</div>
                                {this.props.loading ? (
                                    <Skeleton height={3} />
                                ) : (
                                    <NineBarGraph data={this.state.bars} />
                                )}
                            </Grid.Column>
                            <Grid.Column>
                                <div className="dashboard__label">{this.props.__('Yearly Change')}</div>
                                {this.props.loading ? (
                                    <Skeleton height={3} />
                                ) : (
                                    <div className="dashboard__stat-focus">+127.01</div>
                                )}
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                </div>
            </div>
        );
    }
}

export default withLoadingState(Dashboard);
