import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Line } from 'britecharts-react';

import withResponsiveness from '../../helpers/withResponsiveness';
import ResponsiveContainer from '../../helpers/responsiveContainer';
const ResponsiveLine = withResponsiveness(Line);

class SlimShaddyLine extends Component {
    static propTypes = {
        data: PropTypes.object
    };

    static defaultProps = {
        data: {
            dataByTopic: []
        }
    };

    hasValidData = (props) => {
        return props.data.dataByTopic && props.data.dataByTopic.length;
    }

    render() {
        return (
            <ResponsiveContainer
                render={({ width }) => (
                    <div>
                        {this.hasValidData(this.props) && (
                            <ResponsiveLine
                                data={this.props.data}
                                width={width}
                                height={width / 4}
                                margin={{ top: 0, right: 0, bottom: 0, left: 0 }}
                                lineGradient={['#ffffff', '#ffffff']}
                                lineCurve="linear"
                            />
                        )}
                    </div>
                )}
            />
        );
    }
}

export default SlimShaddyLine;
