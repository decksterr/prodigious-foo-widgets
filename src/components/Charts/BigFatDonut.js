import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Donut } from 'britecharts-react';
import { formatNumber } from '../../helpers/filters';
import { changeSlice } from '../../actions/donut';

import withResponsiveness from '../../helpers/withResponsiveness';
import ResponsiveContainer from '../../helpers/responsiveContainer';
const ResponsiveDonut = withResponsiveness(Donut);

export const colors = {
    red: '#e26f4d',
    yellow: '#ebc85e',
    green: '#4daf7b'
};

class BigFatDonut extends Component {
    static propTypes = {
        data: PropTypes.array
    };

    static defaultProps = {
        data: []
    };

    constructor(props) {
        super(props);
        this.state = {
            activeSlice: props.activeSlice,
            isAnimated: true
        };
    }

    handleDonutText = ({ weight, fileCount }) => {
        return `${formatNumber(weight / Math.pow(10, 6))}Gb ${formatNumber(
            fileCount
        )} ${this.props.__('files')}`;
    };

    handleMouseOver = (...args) => {
        if (this.state.isAnimated) {
            this.setState(() => ({ isAnimated: false }));
        }
        if (this.state.activeSlice !== args[0].data.id) {
            this.props.dispatch(changeSlice(args[0].data));
            this.setState(() => ({ activeSlice: args[0].data.id }));
        }
    };

    handleMouseOut = (...args) => {
        // force active slice to return to default state
        this.props.dispatch(changeSlice(this.props.data[1]));
        this.setState(() => ({ activeSlice: this.props.data[1].id }));
    };

    componentDidMount() {
        if (this.props.data.length) {
            this.props.dispatch(changeSlice(this.props.data[1]));
        }
        this.setState(() => ({ isAnimated: false, activeSlice: this.props.activeSlice }));
    }

    shouldComponentUpdate(nextProps, nextState) {
        // prevent from firing boot animation each and every time component updates
        // should probably tweak this better
        return false;
    }

    render() {
        return (
            <ResponsiveContainer
                render={({ width }) => (
                    <div>
                        <ResponsiveDonut
                            data={this.props.data}
                            width={width}
                            height={width}
                            colorSchema={[colors.green, colors.red, colors.yellow]}
                            externalRadius={width / 2.5}
                            internalRadius={width / 5}
                            isAnimated={this.state.isAnimated}
                            highlightSliceById={this.props.activeSlice}
                            hasFixedHighlightedSlice={true}
                            centeredTextFunction={this.handleDonutText}
                            customMouseOver={this.handleMouseOver}
                            customMouseOut={this.handleMouseOut}
                        />
                    </div>
                )}
            />
        );
    }
}

const mapStateToProps = (state, props) => ({
    activeSlice: state.donut.activeSlice
});

export default connect(mapStateToProps)(BigFatDonut);
