import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Bar } from 'britecharts-react';

import withResponsiveness from '../../helpers/withResponsiveness';
import ResponsiveContainer from '../../helpers/responsiveContainer';
const ResponsiveBarChart = withResponsiveness(Bar);

export const colors = {
    red: '#e26f4d',
    yellow: '#ebc85e',
    green: '#4daf7b'
};

class NineBarGraph extends Component {
    static propTypes = {
        data: PropTypes.array
    };

    static defaultProps = {
        data: []
    };

    render() {
        return (
            <ResponsiveContainer
                render={({ width }) => (
                    <div>
                        <ResponsiveBarChart
                            data={this.props.data}
                            enableLabels={false}
                            hasPercentage={false}
                            width={width}
                            height={width / 4}
                            betweenBarsPadding={0.3}
                            xTicks={0}
                            yTicks={0}
                            margin={{ top: 0, right: 0, bottom: 0, left: 0 }}
                            colorSchema={[
                                colors.green,
                                colors.green,
                                colors.green,
                                colors.green,
                                colors.red,
                                colors.red,
                                colors.red,
                                colors.red,
                                colors.red
                            ]}
                        />
                    </div>
                )}
            />
        );
    }
}

export default NineBarGraph;
