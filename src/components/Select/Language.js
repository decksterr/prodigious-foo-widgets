import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Select } from 'semantic-ui-react';
import { translateTo } from '../../actions/translate';

const languageOptions = [
    {
        key: 'en_GB',
        value: 'en_GB',
        flag: 'gb',
        text: 'English'
    },
    {
        key: 'es_CO',
        value: 'es_CO',
        flag: 'co',
        text: 'Español'
    }
];

class SelectLanguage extends Component {
    static propTypes = {
        languages: PropTypes.array
    };

    static defaultProps = {
        languages: languageOptions
    };

    fetchLanguageSelection = () => {
        if (window.localStorage.language) {
            return languageOptions.filter((languageOption) => {
                return languageOption.value === window.localStorage.language;
            })[0].value;
        }
        return '';
    };

    handleLanguageSelect = (event, element) => {
        this.props.dispatch(translateTo(element.value));
        window.localStorage.setItem('language', element.value);
    };

    render() {
        return (
            <Select
                placeholder="Translation here"
                options={this.props.languages}
                className="select-language"
                defaultValue={this.fetchLanguageSelection()}
                onChange={this.handleLanguageSelect}
            />
        );
    }
}

export default connect()(SelectLanguage);
