export const formatNumber = (value, digits = 0) => {
    return value.toLocaleString(window.navigator.language, { minimumFractionDigits: digits });
};


export default {
    formatNumber
};