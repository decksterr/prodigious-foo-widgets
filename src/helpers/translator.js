import en_GB from '../trans/messages.en_GB.yml';
import es_CO from '../trans/messages.es_CO.yml';

const translations = {
    en_GB,
    es_CO
};

export default (language) => {
    return (message) => {
        // console.log('translator curried:', language);
        if (language && translations[language]) {
            return translations[language][message] || message;
        }
        return message;
    };
};
