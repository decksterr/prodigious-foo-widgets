import { createStore, combineReducers } from 'redux';
import xhr from '../reducers/xhr';
import i18n from '../reducers/translate';
import donut from '../reducers/donut';

export default () => {
    const store = createStore(
        combineReducers({
            xhr,
            i18n,
            donut
        }),
        window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
    );

    return store;
};
